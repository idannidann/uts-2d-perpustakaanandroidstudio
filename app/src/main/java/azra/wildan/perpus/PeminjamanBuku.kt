package azra.wildan.perpus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import azra.wildan.perpus.Adapter.ListPeminjamanAdapter
import azra.wildan.perpus.DBHelper.DBHelper
import azra.wildan.perpus.Model.Peminjaman
import kotlinx.android.synthetic.main.activity_peminjaman_buku.*

class PeminjamanBuku : AppCompatActivity() {

    internal lateinit var db: DBHelper
    internal var lstPinjams:List<Peminjaman> = ArrayList<Peminjaman>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_peminjaman_buku)
        db = DBHelper(this)

        refreshData()

        //Event
        btnTambahPeminjaman.setOnClickListener {
            val pinjam = Peminjaman(
                Integer.parseInt(ed_inidpinjam.text.toString()),
                ed_innamapinjam.text.toString(),
                ed_inbukupinjam.text.toString(),
                ed_intanggalpinjam.text.toString()
            )
            db.addPinjam(pinjam)
            Toast.makeText(this, "Data berhasil di Insert", Toast.LENGTH_SHORT).show()
            refreshData()
        }

        btnUpdatePeminjaman.setOnClickListener {
            val pinjam = Peminjaman(
                Integer.parseInt(ed_inidpinjam.text.toString()),
                ed_innamapinjam.text.toString(),
                ed_inbukupinjam.text.toString(),
                ed_intanggalpinjam.text.toString()
            )
            db.updatePinjam(pinjam)
            Toast.makeText(this, "Data berhasil di Update", Toast.LENGTH_SHORT).show()
            refreshData()
        }

        btnDeletePeminjaman.setOnClickListener {
            val pinjam = Peminjaman(
                Integer.parseInt(ed_inidpinjam.text.toString()),
                ed_innamapinjam.text.toString(),
                ed_inbukupinjam.text.toString(),
                ed_intanggalpinjam.text.toString()
            )
            db.deletePinjam(pinjam)
            Toast.makeText(this, "Data berhasil di Delete", Toast.LENGTH_SHORT).show()
            refreshData()
        }
    }

    private fun refreshData() {
        lstPinjams = db.allPinjam
        val adapter = ListPeminjamanAdapter(this@PeminjamanBuku,lstPinjams,ed_inidpinjam,ed_innamapinjam,ed_inbukupinjam,ed_intanggalpinjam)
        list_peminjaman.adapter = adapter
    }
}
