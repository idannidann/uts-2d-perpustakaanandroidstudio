package azra.wildan.perpus.Model

class Anggota {
    var id_anggota:Int=0
    var email_anggota:String?=null
    var hp_anggota:String?=null
    var nama_anggota:String?=null

    constructor(){}

    constructor(id_anggota:Int,email_anggota:String,hp_anggota:String,nama_anggota:String)
    {
        this.id_anggota=id_anggota
        this.email_anggota=email_anggota
        this.hp_anggota=hp_anggota
        this.nama_anggota=nama_anggota
    }
}