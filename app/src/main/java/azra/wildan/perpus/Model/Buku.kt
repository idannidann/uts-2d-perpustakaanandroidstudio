package azra.wildan.perpus.Model

class Buku {
    var id_buku:Int=0
    var judul_buku:String?=null
    var pengarang_buku:String?=null

    constructor(){}

    constructor(id_buku:Int,judul_buku:String,pengarang_buku:String)
    {
        this.id_buku=id_buku
        this.judul_buku=judul_buku
        this.pengarang_buku=pengarang_buku
    }
}