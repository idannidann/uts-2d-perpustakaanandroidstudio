package azra.wildan.perpus.Model

class Peminjaman {
    var id_pinjam:Int=0
    var nama_pinjam:String?=null
    var buku_pinjam:String?=null
    var tanggal_pinjam:String?=null

    constructor(){}

    constructor(id_pinjam:Int,nama_pinjam:String,buku_pinjam:String,tanggal_pinjam:String)
    {
        this.id_pinjam=id_pinjam
        this.nama_pinjam=nama_pinjam
        this.buku_pinjam=buku_pinjam
        this.tanggal_pinjam=tanggal_pinjam
    }
}