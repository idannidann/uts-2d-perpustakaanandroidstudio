package azra.wildan.perpus.Adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import azra.wildan.perpus.Model.Anggota
import azra.wildan.perpus.R
import kotlinx.android.synthetic.main.activity_list_anggota.view.*

class ListAnggotaAdapter(internal var activity: Activity,
                         internal var lstAnggota: List<Anggota>,
                         internal var ed_inidanggota: EditText,
                         internal var ed_inemail: EditText,
                         internal var ed_inhp: EditText,
                         internal var ed_innama: EditText): BaseAdapter() {

    internal var inflater: LayoutInflater

    init {
        inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView: View
        rowView = inflater.inflate(R.layout.activity_list_anggota,null)

        rowView.txt_id_anggota.text = lstAnggota[position].id_anggota.toString()
        rowView.txt_email_anggota.text = lstAnggota[position].email_anggota.toString()
        rowView.txt_hp_anggota.text = lstAnggota[position].hp_anggota.toString()
        rowView.txt_nama_anggota.text = lstAnggota[position].nama_anggota.toString()

        //event
        rowView.setOnClickListener {
            ed_inidanggota.setText(rowView.txt_id_anggota.text.toString())
            ed_inemail.setText(rowView.txt_email_anggota.text.toString())
            ed_inhp.setText(rowView.txt_hp_anggota.text.toString())
            ed_innama.setText(rowView.txt_nama_anggota.text.toString())
        }
        return rowView
    }

    override fun getItem(position: Int): Any {
        return lstAnggota[position]
    }

    override fun getItemId(position: Int): Long {
        return lstAnggota[position].id_anggota.toLong()
    }

    override fun getCount(): Int {
        return lstAnggota.size
    }
}