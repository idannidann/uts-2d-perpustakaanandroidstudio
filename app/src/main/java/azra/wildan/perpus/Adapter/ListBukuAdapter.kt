package azra.wildan.perpus.Adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import azra.wildan.perpus.Model.Buku
import azra.wildan.perpus.R
import kotlinx.android.synthetic.main.activity_list_buku.view.*

class ListBukuAdapter(internal var activity: Activity,
                      internal var lstBuku: List<Buku>,
                      internal var ed_inidbuku: EditText,
                      internal var ed_judul: EditText,
                      internal var ed_pengarang: EditText): BaseAdapter() {

    internal var inflater: LayoutInflater

    init {
        inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView: View
        rowView = inflater.inflate(R.layout.activity_list_buku,null)

        rowView.txt_id_buku.text = lstBuku[position].id_buku.toString()
        rowView.txt_judul_buku.text = lstBuku[position].judul_buku.toString()
        rowView.txt_pengarang_buku.text = lstBuku[position].pengarang_buku.toString()

        //event
        rowView.setOnClickListener {
            ed_inidbuku.setText(rowView.txt_id_buku.text.toString())
            ed_judul.setText(rowView.txt_judul_buku.text.toString())
            ed_pengarang.setText(rowView.txt_pengarang_buku.text.toString())
        }
        return rowView
    }

    override fun getItem(position: Int): Any {
        return lstBuku[position]
    }

    override fun getItemId(position: Int): Long {
        return lstBuku[position].id_buku.toLong()
    }

    override fun getCount(): Int {
        return lstBuku.size
    }
}