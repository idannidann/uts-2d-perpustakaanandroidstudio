package azra.wildan.perpus.Adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import azra.wildan.perpus.Model.Peminjaman
import azra.wildan.perpus.R
import kotlinx.android.synthetic.main.activity_list_peminjaman.view.*

class ListPeminjamanAdapter(internal var activity: Activity,
                            internal var lstPinjam: List<Peminjaman>,
                            internal var ed_inidpinjam: EditText,
                            internal var ed_innamapinjam: EditText,
                            internal var ed_inbukupinjam: EditText,
                            internal var ed_intanggalpinjam: EditText
): BaseAdapter() {

    internal var inflater: LayoutInflater

    init {
        inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView: View
        rowView = inflater.inflate(R.layout.activity_list_peminjaman,null)

        rowView.txt_id_peminjaman.text = lstPinjam[position].id_pinjam.toString()
        rowView.txt_nama_pinjam.text = lstPinjam[position].nama_pinjam.toString()
        rowView.txt_buku_pinjam.text = lstPinjam[position].buku_pinjam.toString()
        rowView.txt_tanggal_pinjam.text = lstPinjam[position].tanggal_pinjam.toString()

        //event
        rowView.setOnClickListener {
            ed_inidpinjam.setText(rowView.txt_id_peminjaman.text.toString())
            ed_innamapinjam.setText(rowView.txt_nama_pinjam.text.toString())
            ed_inbukupinjam.setText(rowView.txt_buku_pinjam.text.toString())
            ed_intanggalpinjam.setText(rowView.txt_tanggal_pinjam.text.toString())
        }
        return rowView
    }

    override fun getItem(position: Int): Any {
        return lstPinjam[position]
    }

    override fun getItemId(position: Int): Long {
        return lstPinjam[position].id_pinjam.toLong()
    }

    override fun getCount(): Int {
        return lstPinjam.size
    }
}