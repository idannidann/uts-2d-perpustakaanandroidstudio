package azra.wildan.perpus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_menu_utama.*

class MenuUtamaActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_utama)

        btnDataAnggota.setOnClickListener(this)
        btnDataBuku.setOnClickListener(this)
        btnPeminjamanBuku.setOnClickListener(this)
        btnLogout.setOnClickListener(this)
        btnTentang.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDataAnggota ->{
                var intentAnggota = Intent(this, DataAnggota::class.java)
                startActivity(intentAnggota)
            }
            R.id.btnDataBuku ->{
                var intentBuku = Intent(this, DataBuku::class.java)
                startActivity(intentBuku)
            }
            R.id.btnPeminjamanBuku ->{
                var intentPinjam = Intent(this, PeminjamanBuku::class.java)
                startActivity(intentPinjam)
            }
            R.id.btnLogout ->{
                var intentLogout = Intent(this, MainActivity::class.java)
                startActivity(intentLogout)
            }
            R.id.btnTentang ->{
                var intentTentang = Intent(this, Tentang::class.java)
                startActivity(intentTentang)
            }
        }
    }
}
