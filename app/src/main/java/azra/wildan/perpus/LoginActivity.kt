package azra.wildan.perpus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import azra.wildan.perpus.DBHelper.DBHelper
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    internal lateinit var db: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin1.setOnClickListener(this)

        db = DBHelper(this)


        //Event
        btnLogin1.setOnClickListener {
            db.userPresent(edUsernameAdmin.text.toString(), edPasswordAdmin.text.toString())
            Toast.makeText(this, "Login success", Toast.LENGTH_SHORT).show()
            var intentLog1 = Intent(this, MenuUtamaActivity::class.java)
            startActivity(intentLog1)
        }
    }

    override fun onClick(v: View?) {
//        when(v?.id){
//            R.id.btnLogin1 ->{
//                var intentLog1 = Intent(this, MenuUtamaActivity::class.java)
//                startActivity(intentLog1)
//            }
//        }
    }
}
