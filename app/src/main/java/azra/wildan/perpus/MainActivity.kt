package azra.wildan.perpus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin ->{
                var intentLog = Intent(this, LoginActivity::class.java)
                startActivity(intentLog)
            }
            R.id.btnDaftar ->{
                var intentReg = Intent(this, DaftarActivity::class.java)
                startActivity(intentReg)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener(this)
        btnDaftar.setOnClickListener(this)
    }


}
