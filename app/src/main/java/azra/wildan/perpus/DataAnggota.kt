package azra.wildan.perpus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import azra.wildan.perpus.Adapter.ListAnggotaAdapter
import azra.wildan.perpus.DBHelper.DBHelper
import azra.wildan.perpus.Model.Anggota
import kotlinx.android.synthetic.main.activity_data_anggota.*

class DataAnggota : AppCompatActivity() {

    internal lateinit var db: DBHelper
    internal var lstAnggotas:List<Anggota> = ArrayList<Anggota>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_anggota)

        db = DBHelper(this)

        refreshData()

        //Event
        btnTambahAnggota.setOnClickListener {
            val anggota = Anggota(
                Integer.parseInt(ed_inidanggota.text.toString()),
                ed_inemail.text.toString(),
                ed_inhp.text.toString(),
                ed_innama.text.toString()
            )
            db.addAnggota(anggota)
            Toast.makeText(this, "Data berhasil di Insert", Toast.LENGTH_SHORT).show()
            refreshData()
        }

        btnUpdateAnggota.setOnClickListener {
            val anggota = Anggota(
                Integer.parseInt(ed_inidanggota.text.toString()),
                ed_inemail.text.toString(),
                ed_inhp.text.toString(),
                ed_innama.text.toString()
            )
            db.updateAnggota(anggota)
            Toast.makeText(this, "Data berhasil di Update", Toast.LENGTH_SHORT).show()
            refreshData()
        }

        btnDeleteAnggota.setOnClickListener {
            val anggota = Anggota(
                Integer.parseInt(ed_inidanggota.text.toString()),
                ed_inemail.text.toString(),
                ed_inhp.text.toString(),
                ed_innama.text.toString()
            )
            db.deleteAnggota(anggota)
            Toast.makeText(this, "Data berhasil di Delete", Toast.LENGTH_SHORT).show()
            refreshData()
        }
    }

    private fun refreshData() {
        lstAnggotas = db.allAnggota
        val adapter = ListAnggotaAdapter(this@DataAnggota,lstAnggotas,ed_inidanggota,ed_inemail,ed_inhp,ed_innama)
        list_anggota.adapter = adapter
    }
}
