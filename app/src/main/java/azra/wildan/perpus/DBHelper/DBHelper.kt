package azra.wildan.perpus.DBHelper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import azra.wildan.perpus.Model.Anggota
import azra.wildan.perpus.Model.Buku
import azra.wildan.perpus.Model.Peminjaman
import azra.wildan.perpus.Model.User

class DBHelper(context: Context):SQLiteOpenHelper(context,DARABASE_NAME, null,DATABASE_VER) {

    companion object {
        private val DATABASE_VER = 1
        private val DARABASE_NAME = "perpus.db"

        //table anggota
        private val TABLE_ANGGOTA="Anggota"
        private val COL_ID_ANGGOTA="IdAnggota"
        private val COL_EMAIL_ANGGOTA="EmailAnggota"
        private val COL_HP_ANGGOTA="HpAnggota"
        private val COL_NAMA_ANGGOTA="NamaAnggota"

        //table buku
        private val TABLE_BUKU="Buku"
        private val COL_ID_BUKU="IdBuku"
        private val COL_JUDUL_BUKU="JudulBuku"
        private val COL_PENGARANG_BUKU="PengarangBuku"

        //table peminjaman
        private val TABLE_PEMINJAMAN="Peminjaman"
        private val COL_ID_PINJAM="IdPinjam"
        private val COL_NAMA_PINJAM="NamaPinjam"
        private val COL_BUKU_PINJAM="BukuPinjam"
        private val COL_TANGGAL_PINJAM="TanggalPinjam"

        //table user
        private val TABLE_USER="User"
        private val COL_ID_USER="IdUser"
        private val COL_NAME="Name"
        private val COL_EMAIL="Email"
        private val COL_PASSWORD="Password"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE_QUERY = ("CREATE TABLE $TABLE_ANGGOTA ($COL_ID_ANGGOTA INTEGER PRIMARY KEY AUTOINCREMENT, $COL_EMAIL_ANGGOTA TEXT, $COL_HP_ANGGOTA TEXT, $COL_NAMA_ANGGOTA TEXT)")
        val CREATE_TABLE_QUERY1 = ("CREATE TABLE $TABLE_BUKU ($COL_ID_BUKU INTEGER PRIMARY KEY AUTOINCREMENT, $COL_JUDUL_BUKU TEXT, $COL_PENGARANG_BUKU TEXT)")
        val CREATE_TABLE_QUERY2 = ("CREATE TABLE $TABLE_PEMINJAMAN ($COL_ID_PINJAM INTEGER PRIMARY KEY AUTOINCREMENT, $COL_NAMA_PINJAM TEXT, $COL_BUKU_PINJAM TEXT, $COL_TANGGAL_PINJAM DATE)")
        val CREATE_TABLE_QUERY3 = ("CREATE TABLE $TABLE_USER ($COL_ID_USER INTEGER PRIMARY KEY AUTOINCREMENT, $COL_NAME TEXT, $COL_EMAIL TEXT, $COL_PASSWORD TEXT)")
        db!!.execSQL(CREATE_TABLE_QUERY)
        db!!.execSQL(CREATE_TABLE_QUERY1)
        db!!.execSQL(CREATE_TABLE_QUERY2)
        db!!.execSQL(CREATE_TABLE_QUERY3)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_ANGGOTA")
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_BUKU")
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_PEMINJAMAN")
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_USER")
        onCreate(db!!)
    }

    //crud anggota
    val allAnggota:List<Anggota>
        get() {
            val lstAnggotas = ArrayList<Anggota>()
            val selectQuery = "SELECT * FROM $TABLE_ANGGOTA"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery,null)
            if(cursor.moveToFirst()){
                do{
                    val anggota = Anggota()
                    anggota.id_anggota = cursor.getInt(cursor.getColumnIndex(COL_ID_ANGGOTA))
                    anggota.email_anggota = cursor.getString(cursor.getColumnIndex(COL_EMAIL_ANGGOTA))
                    anggota.hp_anggota = cursor.getString(cursor.getColumnIndex(COL_HP_ANGGOTA))
                    anggota.nama_anggota = cursor.getString(cursor.getColumnIndex(COL_NAMA_ANGGOTA))

                    lstAnggotas.add(anggota)
                }while(cursor.moveToNext())
            }
            db.close()
            return lstAnggotas
        }

    fun addAnggota(anggota: Anggota)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_ANGGOTA,anggota.id_anggota)
        values.put(COL_EMAIL_ANGGOTA,anggota.email_anggota)
        values.put(COL_HP_ANGGOTA,anggota.hp_anggota)
        values.put(COL_NAMA_ANGGOTA,anggota.nama_anggota)

        db.insert(TABLE_ANGGOTA,null,values)
        db.close()
    }

    fun updateAnggota(anggota: Anggota):Int
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_ANGGOTA,anggota.id_anggota)
        values.put(COL_EMAIL_ANGGOTA,anggota.email_anggota)
        values.put(COL_HP_ANGGOTA,anggota.hp_anggota)
        values.put(COL_NAMA_ANGGOTA,anggota.nama_anggota)

        return db.update(TABLE_ANGGOTA,values,"$COL_ID_ANGGOTA=?", arrayOf(anggota.id_anggota.toString()))

    }

    fun deleteAnggota(anggota: Anggota)
    {
        val db = this.writableDatabase

        db.delete(TABLE_ANGGOTA,"$COL_ID_ANGGOTA=?", arrayOf(anggota.id_anggota.toString()))
    }


    //crud buku
    val allBuku:List<Buku>
        get() {
            val lstBukus = ArrayList<Buku>()
            val selectQuery = "SELECT * FROM $TABLE_BUKU"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery,null)
            if(cursor.moveToFirst()){
                do{
                    val buku = Buku()
                    buku.id_buku = cursor.getInt(cursor.getColumnIndex(COL_ID_BUKU))
                    buku.judul_buku = cursor.getString(cursor.getColumnIndex(COL_JUDUL_BUKU))
                    buku.pengarang_buku = cursor.getString(cursor.getColumnIndex(COL_PENGARANG_BUKU))

                    lstBukus.add(buku)
                }while(cursor.moveToNext())
            }
            db.close()
            return lstBukus
        }

    fun addBuku(buku: Buku)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_BUKU,buku.id_buku)
        values.put(COL_JUDUL_BUKU,buku.judul_buku)
        values.put(COL_PENGARANG_BUKU,buku.pengarang_buku)

        db.insert(TABLE_BUKU,null,values)
        db.close()
    }

    fun updateBuku(buku: Buku):Int
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_BUKU,buku.id_buku)
        values.put(COL_JUDUL_BUKU,buku.judul_buku)
        values.put(COL_PENGARANG_BUKU,buku.pengarang_buku)

        return db.update(TABLE_BUKU,values,"$COL_ID_BUKU=?", arrayOf(buku.id_buku.toString()))

    }

    fun deleteBuku(buku: Buku)
    {
        val db = this.writableDatabase

        db.delete(TABLE_BUKU,"$COL_ID_BUKU=?", arrayOf(buku.id_buku.toString()))
    }


    //crud peminjaman
    val allPinjam:List<Peminjaman>
        get() {
            val lstPinjams = ArrayList<Peminjaman>()
            val selectQuery = "SELECT * FROM $TABLE_PEMINJAMAN"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery,null)
            if(cursor.moveToFirst()){
                do{
                    val pinjam = Peminjaman()
                    pinjam.id_pinjam = cursor.getInt(cursor.getColumnIndex(COL_ID_PINJAM))
                    pinjam.nama_pinjam = cursor.getString(cursor.getColumnIndex(COL_NAMA_PINJAM))
                    pinjam.buku_pinjam = cursor.getString(cursor.getColumnIndex(COL_BUKU_PINJAM))
                    pinjam.tanggal_pinjam = cursor.getString(cursor.getColumnIndex(COL_TANGGAL_PINJAM))

                    lstPinjams.add(pinjam)
                }while(cursor.moveToNext())
            }
            db.close()
            return lstPinjams
        }

    fun addPinjam(pinjam: Peminjaman)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_PINJAM,pinjam.id_pinjam)
        values.put(COL_NAMA_PINJAM,pinjam.nama_pinjam)
        values.put(COL_BUKU_PINJAM,pinjam.buku_pinjam)
        values.put(COL_TANGGAL_PINJAM,pinjam.tanggal_pinjam)

        db.insert(TABLE_PEMINJAMAN,null,values)
        db.close()
    }

    fun updatePinjam(pinjam: Peminjaman):Int
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_PINJAM,pinjam.id_pinjam)
        values.put(COL_NAMA_PINJAM,pinjam.nama_pinjam)
        values.put(COL_BUKU_PINJAM,pinjam.buku_pinjam)
        values.put(COL_TANGGAL_PINJAM,pinjam.tanggal_pinjam)

        return db.update(TABLE_PEMINJAMAN,values,"$COL_ID_PINJAM=?", arrayOf(pinjam.id_pinjam.toString()))

    }

    fun deletePinjam(pinjam: Peminjaman)
    {
        val db = this.writableDatabase

        db.delete(TABLE_PEMINJAMAN,"$COL_ID_PINJAM=?", arrayOf(pinjam.id_pinjam.toString()))
    }

    //query user
    fun addUser(user: User)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_NAME,user.name)
        values.put(COL_EMAIL,user.email)
        values.put(COL_PASSWORD,user.password)

        db.insert(TABLE_USER,null,values)
        db.close()
    }

    fun userPresent(name: String, password: String): Boolean {
        val db = this.writableDatabase
        val query = "select * from $TABLE_USER where $COL_NAME = '$name' and $COL_PASSWORD = '$password'"
        val cursor = db.rawQuery(query, null)
        if(cursor.count <= 0) {
            cursor.close()
            return false
        }
        cursor.close()
        return true
    }
}

