package azra.wildan.perpus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import azra.wildan.perpus.DBHelper.DBHelper
import azra.wildan.perpus.Model.User
import kotlinx.android.synthetic.main.activity_daftar.*

class DaftarActivity : AppCompatActivity() {

    internal lateinit var db: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar)

//        btnDaftarAdmin.setOnClickListener(this)

        db = DBHelper(this)


        //Event
        btnDaftarAdmin.setOnClickListener {
            val user = User(
                edUsername.text.toString(),
                edEmail.text.toString(),
                edPassword.text.toString()
            )
            db.addUser(user)
            Toast.makeText(this, "Akun berhasil terdaftar", Toast.LENGTH_SHORT).show()
            showLogin()
        }
    }

    private fun showLogin(){
        setContentView(R.layout.activity_login)
    }


//    override fun onClick(v: View?) {
//        when(v?.id){
//            R.id.btnDaftarAdmin ->{
//                var intentDaftar = Intent(this, LoginActivity::class.java)
//                startActivity(intentDaftar)
//            }
//        }
//    }
}
