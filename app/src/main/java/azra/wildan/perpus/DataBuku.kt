package azra.wildan.perpus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import azra.wildan.perpus.Adapter.ListBukuAdapter
import azra.wildan.perpus.DBHelper.DBHelper
import azra.wildan.perpus.Model.Buku
import kotlinx.android.synthetic.main.activity_data_buku.*

class DataBuku : AppCompatActivity() {

    internal lateinit var db: DBHelper
    internal var lstBukus:List<Buku> = ArrayList<Buku>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_buku)

        db = DBHelper(this)

        refreshData()

        //Event
        btnTambahBuku.setOnClickListener {
            val buku = Buku(
                Integer.parseInt(ed_inidbuku.text.toString()),
                ed_judul.text.toString(),
                ed_pengarang.text.toString()
            )
            db.addBuku(buku)
            Toast.makeText(this, "Data berhasil di Insert", Toast.LENGTH_SHORT).show()
            refreshData()
        }

        btnUpdateBuku.setOnClickListener {
            val buku = Buku(
                Integer.parseInt(ed_inidbuku.text.toString()),
                ed_judul.text.toString(),
                ed_pengarang.text.toString()
            )
            db.updateBuku(buku)
            Toast.makeText(this, "Data berhasil di Update", Toast.LENGTH_SHORT).show()
            refreshData()
        }

        btnDeleteBuku.setOnClickListener {
            val buku = Buku(
                Integer.parseInt(ed_inidbuku.text.toString()),
                ed_judul.text.toString(),
                ed_pengarang.text.toString()
            )
            db.deleteBuku(buku)
            Toast.makeText(this, "Data berhasil di Delete", Toast.LENGTH_SHORT).show()
            refreshData()
        }
    }

    private fun refreshData() {
        lstBukus = db.allBuku
        val adapter = ListBukuAdapter(this@DataBuku,lstBukus,ed_inidbuku,ed_judul,ed_pengarang)
        list_buku.adapter = adapter
    }
}
